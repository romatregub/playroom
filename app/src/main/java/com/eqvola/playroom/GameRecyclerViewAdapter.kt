package com.eqvola.playroom

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import android.text.method.TextKeyListener.clear
import android.arch.lifecycle.ViewModel
import android.text.method.TextKeyListener.clear
import android.support.v7.util.DiffUtil





class GameRecyclerViewAdapter(
    private val gamesList: MutableList<GameItem>,
    private val context: Context,
    private val firestoreDB: FirebaseFirestore
)
    : RecyclerView.Adapter<GameRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.game_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = gamesList[position]

        holder.name.text = item.name
        holder.title.text = item.title
        holder.description.text = item.description

        // Download application image from URL
        GlideApp.with(context)
            .load(item.url)
            .into(holder.image)

        // Set
        holder.button.setOnClickListener { launchItem(position) }

    }

    fun updateData(newItems: MutableList<GameItem>) {

        val diffCallback = GameDiffCallback(this.gamesList, newItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.gamesList.clear()
        this.gamesList.addAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }
    override fun getItemCount(): Int {
        return gamesList.size
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var name: TextView = view.findViewById(R.id.gameItemName)
        internal var title: TextView = view.findViewById(R.id.gameItemTitle)
        internal var image: ImageView = view.findViewById(R.id.gameItemImage)
        internal var description: TextView = view.findViewById(R.id.gameItemDescription)
        internal var button: TextView = view.findViewById(R.id.btnPlay)
    }

    private fun launchItem(position: Int) {

        var launchIntent: Intent? = null

        // Search application on device by id
        gamesList[position].appId?.let {
            launchIntent = context.packageManager?.getLaunchIntentForPackage(gamesList[position].appId!!)
        }

        if (launchIntent != null) {
            context.startActivity(launchIntent)
        }
        else
        {
            // Attempt open play market and find application by id
            try {
                var playStoreUri: Uri = Uri.parse("market://details?id=" + gamesList[position].appId)
                var playStoreIntent = Intent(Intent.ACTION_VIEW, playStoreUri)
                context.startActivity(playStoreIntent)

            }catch (exp:Exception){
                // If market app not exist on device
                var playStoreUriBrowser: Uri = Uri.parse("http://play.google.com/store/apps/details?id=" + gamesList[position].appId)
                var playStoreIntent2 = Intent(Intent.ACTION_VIEW, playStoreUriBrowser)
                context.startActivity(playStoreIntent2)
            }
        }

    }
}