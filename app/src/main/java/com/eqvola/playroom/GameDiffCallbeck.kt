package com.eqvola.playroom

import android.support.annotation.Nullable
import android.support.v7.util.DiffUtil




class GameDiffCallback(private val mOldGameList: List<GameItem>, private val mNewGameeList: List<GameItem>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return mOldGameList.size
    }

    override fun getNewListSize(): Int {
        return mNewGameeList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return mOldGameList[oldItemPosition].id === mNewGameeList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = mOldGameList[oldItemPosition]
        val newItem = mNewGameeList[newItemPosition]

        if(!oldItem.name.equals(newItem.name)) return false
        if(!oldItem.title.equals(newItem.title)) return false
        if(!oldItem.description.equals(newItem.description)) return false
        if(!oldItem.url.equals(newItem.url)) return false
        if(!oldItem.appId.equals(newItem.appId)) return false
        if(!oldItem.conditions.equals(newItem.conditions)) return false

        return true
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}