package com.eqvola.playroom

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.facebook.login.LoginResult
import com.facebook.*
import com.facebook.login.LoginManager
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    private var mAdapter: GameRecyclerViewAdapter? = null

    private var firestoreDB: FirebaseFirestore? = null
    private var firestoreListener: ListenerRegistration? = null

    private var callbackManager = CallbackManager.Factory.create() // Facebook callback

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar) // remove splash theme
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppEventsLogger.activateApp(this.application)

        firestoreDB = FirebaseFirestore.getInstance()

        loadGamesList()

        firestoreListener = firestoreDB!!.collection("Games")
            .addSnapshotListener(EventListener { documentSnapshots, e ->
                if (e != null) {
                    Log.e(TAG, "Listen failed!", e)
                    return@EventListener
                }

                val gamesList = mutableListOf<GameItem>()

                if (documentSnapshots != null) {
                    for (doc in documentSnapshots) {
                        val note = doc.toObject(GameItem::class.java)
                        note.id = doc.id
                        gamesList.add(note)
                    }
                }

                if(mAdapter == null)
                    mAdapter = GameRecyclerViewAdapter(gamesList, applicationContext, firestoreDB!!)
                else
                    mAdapter!!.updateData(gamesList)
                rvGames.adapter = mAdapter
            })

        //////////////
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Logger.d("FBLOGIN", loginResult.accessToken.token.toString())
                Logger.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
                Logger.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())


                val request = GraphRequest.newMeRequest(loginResult.accessToken) { o, response ->
                    try {
                        //here is the data that you want
                        Logger.d("FBLOGIN_JSON_RES", o.toString())

                        if (o.has("id")) {
                            //handleSignInResultFacebook(`object`)
                        } else {
                            Logger.e("FBLOGIN_FAILD", o.toString())
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                        //dismissDialogLogin()
                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "name,email,id,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

            }

            override fun onCancel() {
                Logger.e("FBLOGIN_FAILD", "Cancel")
            }

            override fun onError(error: FacebookException) {
                Logger.e("FBLOGIN_FAILD", "ERROR", error)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        callbackManager?.onActivityResult(requestCode, resultCode, data)

        Log.d("letsSee", "malsehnnnnnn: " + data)
    }

    private fun insertNewGame()
    {
        var game = GameItem()
        game.name = "Knife"
        game.title = "Easy and funny"
        game.description = "Download and play"
        game.url = "https://eqvolasigmablob.blob.core.windows.net/user-avatars/romatregub@gmail.com"

        val db = FirebaseFirestore.getInstance()
        db.collection("Games")
            .add(game.toMap())
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }
    private fun loadGamesList() {
        firestoreDB!!.collection("Games")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val gamesList = mutableListOf<GameItem>()

                    for (doc in task.result!!) {
                        val game = doc.toObject<GameItem>(GameItem::class.java)
                        game.id = doc.id
                        gamesList.add(game)
                    }

                    mAdapter = GameRecyclerViewAdapter(gamesList, applicationContext, firestoreDB!!)
                    val mLayoutManager = LinearLayoutManager(applicationContext)
                    rvGames.layoutManager = mLayoutManager
                    rvGames.itemAnimator = DefaultItemAnimator()
                    rvGames.adapter = mAdapter
                    progressBar.visibility = View.GONE
                    rvGames.visibility = View.VISIBLE
                } else {
                    Log.d(TAG, "Error getting documents: ", task.exception)
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        firestoreListener?.remove()
    }
}
