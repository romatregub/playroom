package com.eqvola.playroom

public class GameItem {

    var id: String? = null
    var name: String? = null
    var title: String? = null
    var appId: String? = null
    var description: String? = null
    var conditions: String? = null
    var url: String? = null

    constructor() {}

    constructor(id: String, name: String, title: String, description: String, conditions: String, url: String, appId: String) {
        this.id = id
        this.name = name
        this.title = title
        this.appId = appId
        this.url = url
        this.description = description
        this.conditions = conditions
    }


    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result.put("id", id!!)
        result.put("title", name!!)
        result.put("title", title!!)
        result.put("description", description!!)
        result.put("url", url!!)
        result.put("appId", appId!!)

        return result
    }
}